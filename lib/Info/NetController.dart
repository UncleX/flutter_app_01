import 'package:flutter/material.dart';
import '../Info/NetCase/Case1Page.dart';
import '../Info/NetCase/Case2Page.dart';
import '../Info/NetCase/HttpClientPage.dart';
import '../Info/NetCase/DioPage.dart';
import '../Info/NetCase/DioChunkPage.dart';
import '../Info/NetCase/WebSocketPage.dart';
import '../Info/NetCase/JsonModelPage.dart';
import '../Info/NetCase/StateTestPage.dart';
import '../Info/NetCase/ProviderStatePage.dart';
import '../Info/NetCase/ProviderStateSecondPage.dart';

class Info extends StatefulWidget {
  @override
  _InfoState createState() => _InfoState();
}

class _InfoState extends State<Info> {

  final List<Text> items=[
    Text('文件操作方式1',style: TextStyle(color: Colors.green,backgroundColor: Colors.red)),
    Text('文件操作方式2'),
    Text('httpClient请求'),
    Text('Dio请求'),
    Text('Dio分块下载,断点续传(待进一步研究)'),
    Text('webSocket 长链接'),
    Text('model解析'),
    Text('状态管理'),
    Text('provider状态管理'),
    Text('provider状态管理2')


  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('网络相关'),
      ),
      body: ListView.builder(
          itemBuilder: Item,
        itemExtent: 50,
        itemCount: items.length,
      ),
    );
  }

  Widget Item(BuildContext context,int index){
    return GestureDetector(
      onTap: (){
        _itemClick(context, index);
      },
      child: Container(
        padding: EdgeInsets.all(5),
        alignment: Alignment.centerLeft,
        color: index.isEven?Colors.red[100]:Colors.blue[100],
        child: items[index],
      ),
    );
  }

  _itemClick(BuildContext context,int index){
    switch (index){
      case 0:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Case1Page()));
        }
        break;
      case 1:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Case2Page()));
        }
        break;
      case 2:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>HttpClientPage()));
        }
        break;
      case 3:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>DioPage()));
        }
        break;
      case 4:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>DioChunkPage()));
        }
        break;
      case 5:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>WebSocketPage()));
        }
        break;

      case 6:
      {
        Navigator.push(context, MaterialPageRoute(builder: (context)=>JsonModelPage()));
      }
      break;
      case 7:
      {
        Navigator.push(context,MaterialPageRoute(builder: (context)=>stateTestPage()));
      }
      break;
      case 8:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>ProviderPage()));
        }
        break;
      case 9:
        {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>ProviderStateSecondPage()));
        }



    }
  }

}
