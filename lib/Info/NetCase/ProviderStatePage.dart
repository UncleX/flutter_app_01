import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';

/// [ChangeNotifier] is a class in `flutter:foundation`. [Counter] does
/// _not_ depend on Provider.
class Counter with ChangeNotifier {
  int _count = 0;
  int get count => _count;

  void increment() {
    _count++;
    notifyListeners();
  }
}

class ProviderPage extends StatefulWidget {
  @override
  _ProviderPageState createState() => _ProviderPageState();
}

class _ProviderPageState extends State<ProviderPage> {

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => Counter(),
      child: Scaffold(
          appBar: AppBar(
            title: Text('provider'),
          ),
          body: Container(
              child: Center(
            child: Container(
                constraints: BoxConstraints.tightFor(width: 200, height: 200),
                //背景装饰
                decoration: BoxDecoration(
                  gradient: RadialGradient(
                    colors: [Colors.red, Colors.blue],
                    center: Alignment.topLeft,
                    radius: 0.98,
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black54,
                      offset: Offset(2.0, 2.0),
                      blurRadius: 20,
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10),
                ),
                transform: Matrix4.rotationZ(.2),
                child: Consumer<Counter>(
                  builder: (context, counter, child) => Text(
                    '${counter.count}',
//                style: Theme.of(context).textTheme.display1,
                  ),
                ),
            ),
          )
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: (){
              print("点击了按钮");
//              在 build 方法中使用，当 notifyListeners 被调用的时候，并不会使 widget 被重构。
              Provider.of<Counter>(context, listen: false).increment();
            },
          ),
        ),
    );
  }
}
