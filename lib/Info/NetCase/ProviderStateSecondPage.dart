import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

class ProviderStateSecondPage extends StatefulWidget {
  @override
  _ProviderStateSecondPageState createState() =>
      _ProviderStateSecondPageState();
}

class Counter with ChangeNotifier {
  int _counter = 0;
  int get counter => _counter;

  void increment() {
    _counter++;
    notifyListeners();
  }
}

class _ProviderStateSecondPageState extends State<ProviderStateSecondPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => Counter(),
      child: Scaffold(
          appBar: AppBar(
            title: Text("state demo"),
          ),
          body: Container(
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  child:Consumer<Counter>(builder: (context, counter, child) {
                  return Column(
                    children: <Widget>[
                      chufaqi(context),
                      jishuqi(counter),
                    ],
                  );
                })),
              ],
            ),
          )),
        
    );
  }

  GestureDetector chufaqi(BuildContext context) {
    return GestureDetector(
                      onTap: () {
                        print('xxxxxx');
                        Provider.of<Counter>(context, listen: false)
                            .increment();
                      },
                      child: Container(
                        width: 100,
                        height: 100,
                      ),
                    );
  }

  Container jishuqi(Counter counter) {
    return Container(
                      padding: EdgeInsets.all(20),
                      width: 100,
                      height: 100,
                      color: Colors.yellow,
                      child: Text('${counter._counter},',
                          style: TextStyle(
                            backgroundColor: Colors.green,
                            fontSize: 30,
                          )),
                    );
  }
}
