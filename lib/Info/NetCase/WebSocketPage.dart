import 'package:flutter/material.dart';
import 'package:web_socket_channel/io.dart';

class WebSocketPage extends StatefulWidget {
  @override
  _WebSocketPageState createState() => _WebSocketPageState();
}

class _WebSocketPageState extends State<WebSocketPage> {
  String _text = "这里将显示回传数据";
  //创建输入框
  TextEditingController _controller = TextEditingController();
  //创建长链接 channel 渠道实例
  IOWebSocketChannel channel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    channel = IOWebSocketChannel.connect('ws://echo.websocket.org');
  }

  @override
  void dispose() {
    //关闭长链接
    channel.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('websocket 长链接'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: <Widget>[
              Form(
                  child: TextFormField(
                controller: _controller,
                decoration: InputDecoration(labelText: '占位文字'),
              )),


              StreamBuilder(
                stream: channel.stream,
                builder: (context, snapShort) {
                  if (snapShort.hasError) {
                    _text = '网络不通';
                  } else if (snapShort.hasData) {
                    _text = 'echo:' + snapShort.data;
                  }
                  return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 24),
                      child: Text('$_text'),
                    );
                },
              )


            ],
          ),
      ),


      floatingActionButton: FloatingActionButton(
        onPressed: _sendMessage,
        tooltip: '发送数据',
        child: Icon(Icons.send),
      ),
    );
  }

  _sendMessage() {
    //判断如果输入框的数据不为空
    if (_controller.text.isNotEmpty) {
      channel.sink.add(_controller.text);
    }
  }
}
