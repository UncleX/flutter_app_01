import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';

class HttpClientPage extends StatefulWidget {
  @override
  _HttpClientPageState createState() => _HttpClientPageState();
}

class _HttpClientPageState extends State<HttpClientPage> {
  bool _isLoading = false;
  String _text = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('httpClent 请求'),
      ),
      body: ConstrainedBox(
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              RaisedButton(
                  child: Text('获取百度首页内容'),
                  onPressed: _isLoading
                      ? null
                      : () async {
                          _isLoading = true;
                          setState(() {
                            _text = '正在请求...';
                          });

                          try {
                            HttpClient client = HttpClient();
                            //打开Http连接
                            HttpClientRequest request = await client
                                .getUrl(Uri.parse("http://www.baidu.com"));
                            //使用iPhone的UA
                            request.headers.add("user-agent",
                                "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1");
                            //等待连接服务器（会将请求信息发送给服务器）
                            HttpClientResponse response = await request.close();
                            //读取响应内容
                            _text =
                                await response.transform(utf8.decoder).join();
                            //输出响应头
                            print(response.headers);

                            //关闭client后，通过该client发起的所有请求都会中止。
                            client.close();
                          } catch (e) {
                            print('请求失败 $e');
                          } finally {
                            setState(() {
                              _isLoading = false;
                            });
                          }
                        }),
              Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: Text(_text.replaceAll(RegExp(r"\s"), "")))
            ],
          ),
        ),
      ),
    );
  }
}
