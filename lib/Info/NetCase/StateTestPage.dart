import 'package:flutter/material.dart';

class stateTestPage extends StatefulWidget {
  @override
  _stateTestPageState createState() => _stateTestPageState();
}

class _stateTestPageState extends State<stateTestPage> {
  //计数器
  int _counter = 0;

  //计数增加的方法
  void _incremen() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('state状态管理')),
      body: Container(
        color: Colors.grey[200],
        child: Center(
          child: Container(
//            width: 100,
//            height: 100,
            //向外填充
            margin: EdgeInsets.all(10),
            //指定宽高
            constraints: BoxConstraints.tightFor(width: 200, height: 200),
            //居中
            alignment: Alignment.center,
            //背景装饰
            decoration: BoxDecoration(
                gradient: RadialGradient(
                    colors: [Colors.red, Colors.orange],
                    center: Alignment.topLeft,
                    radius: .98),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black54,
                    blurRadius: 20,
                    offset: Offset(2.0, 2.0),
                  )
                ]),
            //旋转
            transform: Matrix4.rotationZ(.2),

            child: Text(
              "计数:$_counter",
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
        ),
      ),
      floatingActionButton: buildFloatingActionButton(),
    );
  }

  FloatingActionButton buildFloatingActionButton() {
    return FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          _incremen();
          print('点击了按钮$_counter');
        });
  }
}
