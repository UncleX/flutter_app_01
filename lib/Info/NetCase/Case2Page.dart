import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Case2Page extends StatefulWidget {
  @override
  _Case2PageState createState() => _Case2PageState();
}

class _Case2PageState extends State<Case2Page> {
  int _count;

  @override
  void initState() {
    super.initState();
    //从文件读取数据

    _readCount().then((int value) {
      setState(() {
        _count = value ?? 0;
      });
    });
  }

  //点击button是重新绘制页面
  _buttonClicked() async {
    setState(() {
      _count++;
    });
    SharedPreferences prefer = await SharedPreferences.getInstance();
    //存值
    prefer.setInt('count', _count);
  }

  //取值
  Future<int> _readCount() async {
    SharedPreferences prefer = await SharedPreferences.getInstance();
    return prefer.getInt('count');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('shared_preference文件操作'),
      ),
      body: Container(
        color: Colors.blue[100],
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () async {
                  SharedPreferences prefer =
                      await SharedPreferences.getInstance();
                  prefer.remove('count');
                  setState(() {
                    _count = 0;
                  });
                },
                child: Text(
                  '归零',
                  textAlign: TextAlign.center,
                ),
              ),
              Text('点击了第$_count次'),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _buttonClicked,
      ),
    );
  }
}
