import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class DioPage extends StatefulWidget {
  @override
  _DioPageState createState() => _DioPageState();
}

class _DioPageState extends State<DioPage> {
  bool _isLoading = false;
  String _text = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('dio 实现网络请求'),
        ),
        body: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: RaisedButton(
                          child: Text('请求sina数据'),
                          onPressed: _isLoading
                              ? null
                              : () async {
                                  _isLoading = true;

                                  try {
                                    Dio dio = Dio();
                                    Response response =
                                        await dio.get('http://www.sina.com');
                                    setState(() {
                                      _text = response.toString();
                                    });
                                  } catch (error) {
                                    print('请求失败 $error');
                                  } finally {
                                    _isLoading = false;
                                  }
                                }),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: Text(_text),
                )
              ],
            ),
          ),
        ));
  }
}
