import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class Case1Page extends StatefulWidget {
  @override
  _Case1PageState createState() => _Case1PageState();
}

class _Case1PageState extends State<Case1Page> {
  int _count;

  @override
  void initState() {
    super.initState();
    //从文件读取数据
    _readCount().then((int value) {
      //取出数据后重绘
      setState(() {
        _count = value;
      });
    });
  }

  /*悬浮按钮的点击方法*/
  Future<Null> _buttonClicked() async {
    setState(() {
      _count++;
    });
    //点击的时候将数字写入文件
    await (await _getLocalFile()).writeAsString('$_count');
  }

  //获取本地文件
  Future<File> _getLocalFile() async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    return File('$dir/count.txt');
  }

  Future<int> _readCount() async {
    try {
      //取出文件
      File file = await _getLocalFile();
      //读取数据
      String countString = await file.readAsString();
      //返回数据
      return int.parse(countString);
    } on FileSystemException {
      //异常处理
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('文件操作'),
      ),
      body: Container(
        color: Colors.yellow[100],
        child: Center(
          child: Text('点击了按钮$_count 次'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _buttonClicked();
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
