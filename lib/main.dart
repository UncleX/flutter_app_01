import 'package:flutter/material.dart';
import 'package:flutter_app_01/Home/Home.dart';
import 'package:flutter_app_01/Info/NetController.dart';
import 'package:flutter_app_01/Mine/AnimationController.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'demo',
      home: RootNavigationBar(),
    );
  }
}

class RootNavigationBar extends StatefulWidget {
  @override
  _RootNavigationBarState createState() => _RootNavigationBarState();
}

class _RootNavigationBarState extends State<RootNavigationBar> {
  final _navigationBarBackColor = Colors.teal;
//  List <Widget> pages = <Widget>[Home(),Info(),Mine()];
  List <Widget> pages = [];
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();

    pages
    ..add(Home())
    ..add(Info())
    ..add(Mine());
//  pages.add(Home());
//  pages.add(Info());
//  pages.add(Mine());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //底部导航 相当于iOS tabbar
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: _navigationBarBackColor,),
            title: Text('基础控件', style: TextStyle(color: _navigationBarBackColor),),
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.email, color: _navigationBarBackColor,),
            title: Text('网络相关', style: TextStyle(color: _navigationBarBackColor),),
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.pages, color: _navigationBarBackColor,),
            title: Text('动画相关', style: TextStyle(color: _navigationBarBackColor),),
          ),
        ],


        //当前选中的下标
        currentIndex: _currentIndex,
        //如果 icon 和 title 显式设置了颜色,这个就不管用了
        fixedColor: Colors.blue,
        onTap: (int index){
          setState(() {
            _currentIndex = index;
            print('点击第$_currentIndex 个item---'+'${pages[_currentIndex]}}');

          });
        },
      ),
      body: pages[_currentIndex],
    );
  }

}







