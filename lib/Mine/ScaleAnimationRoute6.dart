import 'package:flutter/material.dart';

class ScaleAnimationRoute6 extends StatefulWidget {
  @override
  _ScaleAnimationRoute6State createState() => _ScaleAnimationRoute6State();
}

class _ScaleAnimationRoute6State extends State<ScaleAnimationRoute6>
    with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: const Duration(seconds: 3));
    animation = Tween(begin: 0.0, end: 300.0).animate(controller);
    controller.forward();

    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: GrowTransition(
        child: Image.asset("images/header.png"),
        animation: animation,
      ),
    );
  }
}

class GrowTransition extends StatelessWidget {
  final Widget child;
  final Animation<double> animation;
  GrowTransition({this.child, this.animation});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
          child: child,
          animation: animation,
          builder: (BuildContext context, Widget child) {
            return Container(
              child: child,
              width: animation.value,
              height: animation.value,
            );
          }),
    );
  }
}
