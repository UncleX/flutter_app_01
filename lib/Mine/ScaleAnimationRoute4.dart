import 'package:flutter/material.dart';
class ScaleAnimationRoute4 extends StatefulWidget {
  @override
  _ScaleAnimationRoute4State createState() => _ScaleAnimationRoute4State();
}

class _ScaleAnimationRoute4State extends State<ScaleAnimationRoute4> with SingleTickerProviderStateMixin{

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 3000));
    animation = Tween(begin: 0.0, end: 200.0).animate(controller);
    controller.forward();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('缩放优化3'),),
      body: Container(
        child: AnimatedBuilder(
            animation: animation,
            child: Image.asset("images/header.png"),
            builder: (BuildContext context, Widget child){
              return Center(
                child: Container(
                  width: animation.value,
                  height: animation.value,
                  child: child,
                ),
              );
            }),
      ),
    );
  }
}
