import 'package:flutter/material.dart';
class ScaleAnimationRoute5 extends StatefulWidget {
  @override
  _ScaleAnimationRoute5State createState() => _ScaleAnimationRoute5State();
}

class _ScaleAnimationRoute5State extends State<ScaleAnimationRoute5> with TickerProviderStateMixin{

  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 3000));
    animation = Tween(begin: 0.0, end: 200.0).animate(controller);
    controller.forward();
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: GrowTransition(
        child: Image.asset("images/header.png"),
        animation: animation,
      ),
    );
  }
}


class GrowTransition extends StatelessWidget {
  final Widget child;
  final Animation<double> animation;

  GrowTransition({this.child, this.animation});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: animation,
        builder: (BuildContext context,Widget child){
          return Container(
            child: child,
            width: animation.value,
            height: animation.value,
          );
        },
        child: child,
      ),
    );
  }

}

/*
* Flutter中正是通过这种方式封装了很多动画，如：FadeTransition、ScaleTransition、SizeTransition、FractionalTranslation等，很多时候都可以复用这些预置的过渡类
* 也许你会说这和我们刚开始的示例差不了多少，其实它会带来三个好处：

* 1.不用显式的去添加帧监听器，然后再调用setState() 了，这个好处和AnimatedWidget是一样的。

* 2.动画构建的范围缩小了，如果没有builder，setState()将会在父widget上下文调用，这将会导致父widget的build方法重新调用，而有了builder之后，只会导致动画widget的build重新调用，这在复杂布局下性能会提高。

* 3.通过AnimatedBuilder可以封装常见的过渡效果来复用动画。下面我们通过封装一个GrowTransition来说明，它可以对子widget实现放大动画：

* */


