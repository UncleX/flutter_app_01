import 'package:flutter/material.dart';
class PageRoute1 extends StatefulWidget {
  @override
  _PageRoute1State createState() => _PageRoute1State();
}

class _PageRoute1State extends State<PageRoute1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('转场动画'),
      ),
      body: Center(
        child: Container(
          child: Image.asset("images/tupian.png",width: 250,height: 200,),
          color: Colors.white,
        ),
      ),
    );
  }
}
