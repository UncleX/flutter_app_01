import 'package:flutter/material.dart';

// ignore: must_be_immutable
class StaggerAnimation extends StatelessWidget {
  StaggerAnimation({Key key, this.controller}) : super(key: key) {
    //高度渐变(缩放)animation
    height = Tween<double>(begin: 0, end: 300).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.0, 0.6, curve: Curves.ease)));

    //位移 animation
    padding = Tween<EdgeInsets>(begin: EdgeInsets.only(left: 0),end: EdgeInsets.only(left: 100)).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.0, 0.6,curve: Curves.ease)
    ));
    //颜色渐变animation
    color = ColorTween(begin: Colors.red,end: Colors.blue).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.6, 1.0,curve: Curves.ease)));
  }

  final Animation<double> controller;
  Animation<double> height;
  Animation<EdgeInsets> padding;
  Animation<Color> color;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: controller,
        builder:(BuildContext context,Widget child){
          return Container(
            padding: padding.value,
            alignment: Alignment.bottomCenter,
            child: Row(
              children: <Widget>[
                Container(
                  width: 50,
                  height: height.value,
                  color: color.value,
                ),
              ],

            ),
          );
        });
  }
}
