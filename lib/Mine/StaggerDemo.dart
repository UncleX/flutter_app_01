import 'package:flutter/material.dart';
import 'StaggerAnimation.dart';

class StaggerDemo extends StatefulWidget {
  @override
  _StaggerDemoState createState() => _StaggerDemoState();
}

class _StaggerDemoState extends State<StaggerDemo>
    with TickerProviderStateMixin {
  AnimationController controller;
  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('组合动画')),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          _playAnimation();
        },
        child: Center(
          child: Center(
            child: Container(
              width: 300,
              height: 300,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.black.withOpacity(0.5),
                  )),
              child: StaggerAnimation(
                controller: controller,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _playAnimation() async {
    try {
      await controller.forward().orCancel;
//      await controller.reverse().orCancel;
    } on TickerCanceled {}
  }
}
