import 'package:flutter/material.dart';
class PageRoute2 extends StatefulWidget {
  @override
  _PageRoute2State createState() => _PageRoute2State();
}

class _PageRoute2State extends State<PageRoute2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('转场动画2'),
      ),
      body: Container(
        child: Image.asset("images/shenxianjiejie.png"),
        color: Colors.white,
      ),
    );
  }
}
