import 'package:flutter/material.dart';

class AnimatedImage extends AnimatedWidget {
  AnimatedImage({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);
  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return new Center(
      child: Image.asset("images/header.png",
          width: animation.value, height: animation.value),
    );
  }
}

class ScaleAnimationRoute3 extends StatefulWidget {
  @override
  _ScaleAnimationRoute3State createState() => _ScaleAnimationRoute3State();
}

class _ScaleAnimationRoute3State extends State<ScaleAnimationRoute3>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 3000));
    animation = Tween(begin: 0.0, end: 200.0).animate(controller);
    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('缩放动画优化2'),
      ),
      body: Center(
        child: AnimatedImage(animation: animation),
      )
    );
  }
}
