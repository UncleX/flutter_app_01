import 'package:flutter/material.dart';
class ScaleAnimationRoute2 extends StatefulWidget {
  @override
  _ScaleAnimationRoute2State createState() => _ScaleAnimationRoute2State();
}

class _ScaleAnimationRoute2State extends State<ScaleAnimationRoute2> with TickerProviderStateMixin{

  AnimationController controller;
  AnimationController controller2;


  @override
  initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: const Duration(seconds: 3),lowerBound: 0.0,upperBound: 300.0);
    //使用弹性曲线
    controller.addListener(() {
      setState(() {});
    });
    controller.forward();


    controller2 = AnimationController(vsync: this, duration: const Duration(seconds: 5),lowerBound: 50.0,upperBound: 400.0);
    //使用弹性曲线
    controller2.addListener(() {
      setState(() {});
    });
    controller2.forward();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("缩放动画优化"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Image.asset("images/header.png",
                    width: controller.value,
                    height: controller.value,
            ),
            Container(
              color: Colors.blue,
              width: controller2.value,
              height: controller2.value,
            )
          ],
        ),
      ),
    );
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

}
