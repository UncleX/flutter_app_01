import 'package:flutter/material.dart';
import 'ScaleAnimationRoute.dart';
import 'ScaleAnimationRoute2.dart';
import 'ScaleAnimationRoute3.dart';
import 'ScaleAnimationRoute4.dart';
import 'ScaleAnimationRoute5.dart';
import 'ScaleAnimationRoute6.dart';
import 'PageRoute1.dart';
import 'PageRoute2.dart';
import 'PageRoute3.dart';
import 'StaggerDemo.dart';

class Mine extends StatefulWidget {
  @override
  _MineState createState() => _MineState();
}

class _MineState extends State<Mine> {
  //定义一个数组
  final List<Text> items = [
    Text('平移动画'),
    Text('缩放动画'),
    Text('缩放动画优化'),
    Text('缩放动画优化2'),
    Text('缩放动画优化3'),
    Text('缩放动画优化4'),
    Text('缩放动画优化5'),
    Text('转场动画'),
    Text('转场动画2'),
    Text('转场动画3'),
    Text('旋转动画'),
    Text('组合动画'),
    Text('自定义动画')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('动画相关'),
        ),
        body: ListView.builder(
          itemBuilder: item,
          itemCount: items.length,
          itemExtent: 50,
        ));
  }

  Widget item(BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        _clickItem(context, index);
      },
      child: Container(
        color: (index % 2 == 0) ? Colors.blue[100] : Colors.red[100],
        padding: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,
        child: items[index],
      ),
    );
  }

  void _clickItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        {
          //do nothing
        }
        break;
      case 1:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute()));
        }
        break;
      case 2:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute2()));
        }
        break;
      case 3:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute3()));
        }
        break;
      case 4:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute4()));
        }
        break;
      case 5:
        {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute5()));
        }
        break;
      case 6:
        {
          //正常的路由切换方式(iOS)
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => ScaleAnimationRoute6()));
        }
        break;

      case 7:
        {
          //自定义路由切换方式一
          Navigator.push(context, PageRouteBuilder(
              pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation){
                //返回渐隐渐现动画效果
                return FadeTransition(
                    opacity: animation,
                    child: PageRoute1()
                );
              },
            //动画时长
            transitionDuration: const Duration(seconds: 2),
          ));
        }
        break;
      case 8:
        {
          Navigator.push(context, CustomRoute(builder: (context){
            return PageRoute2();
          }));
        }
        break;
      case 9:
        {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => StaggerDemo()));
        }
        break;
    }
  }
}


class CustomRoute extends PageRoute{

  CustomRoute({
    @required this.builder,
    this.transitionDuration = const Duration(milliseconds: 300),
    this.opaque = true,
    this.barrierDismissible = false,
    this.barrierColor,
    this.barrierLabel,
    this.maintainState = true});

  final WidgetBuilder builder;

  @override
  final Duration transitionDuration;

  @override
  final bool opaque;

  @override
  final bool barrierDismissible;

  @override
  final Color barrierColor;

  @override
  final String barrierLabel;

  @override
  final bool maintainState;


  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) => builder(context);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(
      opacity: animation,
      child: builder(context),
    );
  }


}
