import 'package:flutter/material.dart';
class Case1Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('最简单的list'),
      ),
      body: ListView(
        //是否根据子 widget 的高度来设置ListView 的高度
        shrinkWrap: true,
        //距离四周的边界
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Text('第1行'),
          Text('第2行'),
          Text('第3行'),
          Text('第4行'),
          Text('第5行'),
          Text('第6行'),
          Text('第7行'),

        ],
      ),
    );
  }


}
