import 'package:flutter/material.dart';
class Case6page extends StatefulWidget {
  @override
  _Case6pageState createState() => _Case6pageState();
}

class _Case6pageState extends State<Case6page> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
//        appBar: AppBar(
//          title: Text('hh'),
//        ),
        body: CustomScrollView(
          slivers: <Widget>[
            //导航栏
            SliverAppBar(
              //固定 属性
              pinned: true,
              //高度
              expandedHeight: 250,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text(''),
                background: Container(
                  color: Colors.green,
                ),
              ),
            ),

            SliverPadding(
              padding: EdgeInsets.all(8.0),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  //2 列
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 20,
                  childAspectRatio: 4.0,
                ),
                delegate: SliverChildBuilderDelegate(
                      (context, index) {
                    return Container(
                      alignment: Alignment.center,
                      color: Colors.cyan[100*(index%9)],
                      child: Text('颜色'),
                    );
                  },
                  childCount: 20,
                ),
              ),
            ),

            SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate((context,index){
                return Container(
                  child: Text('另一个list'),
                  color: Colors.blue[100 *(index%9)],
                  alignment: Alignment.center,
                );
              },
                childCount: 15,
              ),
              //行高
              itemExtent: 50,

            )
          ],
        ),
      ),

    );
  }
}


