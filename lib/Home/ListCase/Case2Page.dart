import 'package:flutter/material.dart';
class Case2page extends StatelessWidget {

  final Widget blueLine = Divider(height: 1, color: Colors.blue,);
  final Widget redLine = Divider(color: Colors.red,height: 1,);

  @override
  Widget build(BuildContext context) {
    /*带分割线的列表*/
    return Container(
      color: Colors.yellow,
      child: ListView.separated(
        shrinkWrap: true,
//      itemExtent:50,//不能设置此属性了
        itemBuilder: (BuildContext context, int index){
          return Text('$index');
        },
        separatorBuilder: (BuildContext context, int index){
          return index%2==0 ? redLine:blueLine;
        },
        itemCount: 10,
      ),
    );
  }
}