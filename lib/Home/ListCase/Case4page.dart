import 'package:flutter/material.dart';
class Case4page extends StatelessWidget {
  /*
    * 当列表滚动到具体的index位置时，会调用该构建器构建列表项。
    * itemCount：列表项的数量，如果为null，则为无限列表。
    * */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView.builder'),
      ),
      body: Container(
        child: ListView.builder(
            shrinkWrap: true,//根据行高计算listView高度
            itemCount: 100,//行数100
            itemExtent: 50,//强制行高50
            itemBuilder: (BuildContext context, int index){
              return ListTile(
                title: Text('$index'),
              );
            }),
      ),
    );
  }
}
