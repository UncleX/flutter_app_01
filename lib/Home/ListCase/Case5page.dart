import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
class Case5page extends StatefulWidget {
  @override
  _Case5pageState createState() => _Case5pageState();
}

class _Case5pageState extends State<Case5page> {
  static const String loadingEndTag = "##loadingEndTag##";
  //数组
  var _words = <String>[loadingEndTag];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('带固定header的list'),
      ),
      body: Column(
        children: <Widget>[
          ListTile(
            title: Text('英文单词'),
          ),
          Expanded(
            child: Container(
              child: ListView.separated(
                itemBuilder: (context, index) {
                  //如果到了表尾
                  if (_words[index] == loadingEndTag) {
                    if (_words.length - 1 < 100) {
                      //获取数据
                      _createDate();
                      //加载时显示loading
                      return Center(
                        child: SizedBox(
                          width: 24,
                          height: 24,
                          child: CircularProgressIndicator(
                            strokeWidth: 2.0,
                          ),
                        ),
                      );
                    } else {
                      return Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(16),
                        child: Text(
                          '没有更多数据了',
                          style: TextStyle(color: Colors.green),
                        ),
                      );
                    }
                  }
                  return ListTile(
                    title: Text('${_words[index].toString()}'),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider();
                },
                itemCount: _words.length,
              ),
            ),
          ),
        ],
      ),
    );
  }

  //随机生成 20 个单词
  void _createDate() {
    Future.delayed(Duration(seconds: 1)).then((e) {
      _words.insertAll(
        _words.length - 1,
        generateWordPairs().take(10).map((e) => e.asPascalCase).toList(),
      );

      setState(() {});
    });
  }
}
