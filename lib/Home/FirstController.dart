import 'package:flutter/material.dart';
import 'package:flutter_app_01/Home/ListCase//Case1page.dart';
import 'package:flutter_app_01/Home/ListCase/Case2Page.dart';
import '../Home/ListCase/Case4page.dart';
import '../Home/ListCase/Case5page.dart';
import '../Home/ListCase/Case6page.dart';
class FirstPage extends StatelessWidget {
  final List<Text> items = [
    Text('简单list'),
    Text('带下划线list'),
    Text('ListView.builder'),
    Text('带固定header的list'),
    Text('多列表混合'),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemBuilder: buildItem,
        itemCount: items.length,
        itemExtent: 50,
      ),
    );
  }

  Widget buildItem(BuildContext context, int index) {
    //设置分割线
    Divider();
    //设置字体样式
    TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0);
    return  GestureDetector(
      onTap: () {
        _test(context,index);
      },
      child:  Container(
        padding: const EdgeInsets.all(8.0),
        child: items[index],
      ),
    );
  }

  _test(BuildContext context,int index) {
    switch (index) {
      case 0:
        {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context)=>Case1Page())
          );
        }
        break;
      case 1:
        {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context)=>Case2page())
          );
        }
        break;
      case 2:
        {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context)=>Case4page())
          );
        }
        break;
      case 3:
        {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context)=>Case5page())
          );
        }
        break;
      case 4:
        {
          print('点击了第4行');
          Navigator.push(
              context,
              MaterialPageRoute(builder:(context)=>Case6page())
          );
        }
        break;
      default:
        {
        }
        break;
    }
  }
}
