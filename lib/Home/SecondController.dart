
import 'package:flutter/material.dart';
class SecondPage extends StatelessWidget {
  SecondPage({Key key}):super(key:key);

  @override
  Widget build(BuildContext context) {

    return GridViewPage();
  }
}
//网格列表
class GridViewPage extends StatefulWidget {
  @override
  _GridViewPageState createState() => _GridViewPageState();
}

class _GridViewPageState extends State<GridViewPage> {
  //用来存储数据
  List<IconData> dataArray =  [];

  @override
  void initState() {
    super.initState();
    _createData();
  }


  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      //网格布局
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          childAspectRatio: 1,
        ),

        //行数
        itemCount: dataArray.length,
        itemBuilder: (context,index){
          if(index == dataArray.length - 1 && dataArray.length < 200 ){
            _createData();
          }
          return Icon(dataArray[index]);
        });
  }

  void _createData(){
    //200 毫秒
    Future.delayed(Duration(milliseconds: 500)).then(
            (e){
          setState(() {
            dataArray.addAll([
              Icons.ac_unit,
              Icons.airport_shuttle,
              Icons.all_inclusive,
              Icons.beach_access, Icons.cake,
              Icons.free_breakfast
            ]);
          });
        }
    );
  }
}
