import 'package:flutter/material.dart';
import 'package:flutter_app_01/Home/FirstController.dart';
import 'package:flutter_app_01/Home/SecondController.dart';
import 'package:flutter_app_01/Home/ThirdPage.dart';
class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>
    with SingleTickerProviderStateMixin {
  var _selectedIndex = 1;
  //第一步:定义全局key
//  static GlobalKey<ScaffoldState> _globalKey = GlobalKey();

  TabController _tabController;
//  List tabs  = ["新闻","历史","图片"];

  List<Tab> tabs = [
    Tab(
      text: 'List',
    ),
    Tab(
      text: 'GridView',
    ),
    Tab(
      text: 'Others',
    ),


  ];
  List<Widget> widgets = [FirstPage(),SecondPage(),ThirdPage()];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: tabs.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //第二步:key = 全局key
//      key: _globalKey,
      appBar: AppBar(
        //1.标题
        title: Text('FLUTTER'),

        //2.按钮(右侧)
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.share),
              onPressed: () {
                print('点击了分享按钮');
              }),
        ],

        //3.自定义左侧返回按钮
        leading: Builder(builder: (context) {
          print('++++$context');
          return IconButton(
              icon: Icon(Icons.dashboard),
              onPressed: () {
                //可以获取父级最近的Scaffold Widget的State对象 (还可以通过全局key的方式来获取state打开侧边栏)
                Scaffold.of(context).openDrawer();
                //第三步:取出全局key 打开侧边栏
//              _globalKey.currentState.openDrawer();
//                print('====${Scaffold.of(context)}');
              });
        }),

        //4.bottom 生成tabBar菜单栏
        bottom: TabBar(
          controller: _tabController,
          //todo:不明白(根据字符串数组生成一个tab的widget数组)
//          tabs: tabs.map((e)=> Tab(text: e,)).toList()
          tabs: tabs,
        ),
      ),

      //主干部分
      body: TabBarView(
        controller: _tabController,
//        children: tabs.map((Tab tab) {
////          return Center(
////              child: Text(
////                tab.text,
////                //缩放因子
////                textScaleFactor: 2,
////          ));
//        }).toList(),
        children: widgets,
      ),

      //侧边栏 (抽屉) 会自动加上左侧按钮,如果已经自定义了返回按钮,这里就会被覆盖使用自定义的按钮.
      //左侧侧滑栏
      drawer: MyDrawer(),
      //右侧侧滑栏
      endDrawer: MyDrawer(),


//    BottomAppBar(
//      color: Colors.white,
//      shape: CircularNotchedRectangle(),//导航栏底部打一个圆形的洞
//      child: Row(
//        children: <Widget>[
//          IconButton(icon: Icon(Icons.home),),
//          SizedBox(),
//          IconButton(icon: Icon(Icons.business), onPressed: null),
//        ],
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//      ),
//    ),

      //悬浮按钮
      floatingActionButton: FloatingActionButton(
        onPressed: _onAdd,
        child: Icon(Icons.add),
      ),

      //这一句决定了floatingActionButton 在dock栏上的位置
//      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  void _onItemTaped(int index) {
    setState(() {
      _selectedIndex = index;
    });

  }

  void _onAdd() {
    print('点击加号按钮');
  }
}

//侧边栏
class MyDrawer extends StatelessWidget {
  const MyDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 38),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: ClipOval(
                        child: Image.asset(
                          'images/header.png',
                          width: 80,
                          height: 80,
                        ),
                      ),
                    ),
                    Text(
                      'wendux',
                      style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      leading: IconButton(
                        icon: Icon(Icons.home),
                        onPressed: null,
                      ),
                      title: Text('home'),
                    ),
                    ListTile(
                      leading:
                      IconButton(icon: Icon(Icons.business), onPressed: null),
                      title: Text('公司'),
                      subtitle: Text('普华商业集团'),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

